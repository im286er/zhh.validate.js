
$(function() {
	// 初始化
	$("form").zhhvalidate("init");
	// 提交测试
	$("form").on("submit", function() { alert("提交"); return false; });
});

$(function() {
	// 自己处理错误提示
	$("button[name=dosubmit]").click(function(e) {
		e.preventDefault();
		var $form = $(this).closest("form");
		$form.zhhvalidate({ tips:false }, function(e) {
			if (e.passed) {
				$form.submit();
			} else {
				alert(e.text);
			}
		});
	});
});

// 自定义校验规则
(function(zv) {
	// 不能小于当前年份的自定义规则
	zv.rules.put("year-less-current", function(e) {
		if (parseInt(e.value || 0) < new Date().getFullYear()) {
			return false; // 校验未通过
		}
		return true; // 校验通过
	});
	zv.locals["zh-CN"]["year-less-current"] = "{label}不能小于今年";
})(jQuery.fn.zhhvalidate.defaults);
